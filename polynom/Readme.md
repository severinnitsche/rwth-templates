# Polynom Template

This template allows for 16:9 Beamer Presentation with a fresh look.

See more [docu pdf](docu_polynom.pdf).
