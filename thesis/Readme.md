# Thesis
Vorlage zur Verwendung bei Abschlussarbeiten (Bachelor, Master).

## Allgemeine Informationen
Die vorliegende Struktur basiert auf dem I7-Seminar-Template sowie meiner Assignment-Klasse.

## Verwendung

### Befehle

- `\university{<Uni>}` legt die Universität fest.
- `\topic{<Thema>}` legt den Titel der Arbeit fest.
- `\thesistype{<Typ>}` legt die Art der Arbeit fest (BA,MA,...).
- `\summary{<Zusammenfassung>}` legt den Inhalt der Zusammenfassung fest. Es wird empfohlen diese aber in die separate Datei `abstract.tex` zu schreiben.
- `\deadline{<Abgabe>}` legt das Datum der Abgabe fest.

### Umgebungen

Über das Paket werden folgende Umgebungen definiert:
- `theorem` für Sätze
- `proposition` für Annahmen
- `lemma` für Lemmata
- `corollary` für Korollare
- `conjecture` für Vermutungen
- `claim` für Behauptungen
- `definition` für Definitionen
- `remark` für Beobachtungen

Alle Umgebungen verwenden denselben Zähler basierend auf der Abschnittsnummerierung.
