\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}

\LoadClass[a4paper,11pt,DIV=15]{scrartcl} % KOMA-Script article class

%% Student information
\edef\@student{}
\newcommand{\student}[2]{\ifx\@student\empty\edef\@student{#1 & #2}\else\edef\@student{#1 & #2 \\\@student{}}\fi}

%% Course information
\newcommand{\@university}{RWTH Aachen University}
\newcommand{\@topic}{{\em No topic specified}}
\newcommand{\@thesistype}{{\em No thesis type specified}}
\newcommand{\@abstract}{{\em No summary specified}}

\newcommand{\university}[1]{\renewcommand{\@university}{#1}}
\newcommand{\topic}[1]{\renewcommand{\@topic}{#1}}
\newcommand{\thesistype}[1]{\renewcommand{\@thesistype}{#1}}
\newcommand{\summary}[1]{\renewcommand{\@abstract}{#1}}

%% Date format
\RequirePackage[ngerman]{babel}
\RequirePackage{datetime}
\RequirePackage{expl3}
\RequirePackage{xparse}
\ExplSyntaxOn
\NewExpandableDocumentCommand{\lasttwoofyear}{m}
 {% #1 is the offset
  \egreg_lasttwo:f { \int_to_arabic:n { #1 } }
 }
\cs_new:Nn \egreg_lasttwo:n
 {
  \tl_range:nnn { #1 } { -2 } { -1 }
 }
\cs_generate_variant:Nn \egreg_lasttwo:n { f }
\ExplSyntaxOff
\newcommand{\@semester}{%
  \ifnum\THEMONTH<4%
    WS~\number\numexpr\THEYEAR-1\relax/\lasttwoofyear{\THEYEAR}
  \else\ifnum\THEMONTH<10%
    SS~\THEYEAR
  \else
    WS~\THEYEAR/\lasttwoofyear{\THEYEAR+1}
  \fi\fi%
}
\newcommand{\@deadline}{\today}
\newcommand{\deadline}[3]{\renewcommand{\@deadline}{\formatdate{#1}{#2}{#3}}}
\newdateformat{unidate}{\THEDAY.~\monthname[\THEMONTH],~\@semester}

%% Typesetting
\renewcommand{\familydefault}{\sfdefault}
\renewcommand{\theequation}{\arabic{enumi}-\arabic{equation}}

%% Theorem counters
\newcounter{theorem}
\let\@section\section
\let\@subsection\subsection
\let\@thesection\thesection
% \let\@thesubsection\thesubsection
\renewcommand{\section}[1]{%
  \counterwithout{theorem}{subsection}%
  \counterwithin{theorem}{section}%
  \renewcommand{\thesection}{\Roman{section}}%
  %\renewcommand{\thesubsection}{\thesection.\Roman{subsection}}
  \@section{#1}%
  \renewcommand{\thesection}{\@thesection}%
  %\renewcommand{\thesubsection}{\@thesubsection}
}
\renewcommand{\subsection}[1]{%
  \counterwithout{theorem}{section}%
  \counterwithin{theorem}{subsection}%
  \renewcommand{\thesection}{\Roman{section}}%
  % \renewcommand{\thesubsection}{\thesection.\Roman{subsection}}
  \@subsection{#1}%
  \renewcommand{\thesection}{\@thesection}%
  % \renewcommand{\thesubsection}{\@thesubsection}
}

\AtBeginDocument{
  \pagenumbering{gobble}
  \title{\@topic}
  \subtitle{\@thesistype}
  \date{\unidate\@deadline}
  \publishers{\@university}
  \author{
    \begin{tabular}{l l}
      \@student
    \end{tabular}
  }
  \maketitle

  \begin{abstract}
    \@abstract
  \end{abstract}

  \thispagestyle{empty}
  \clearpage
  \pagenumbering{arabic}
}

%% Page style
\RequirePackage{lastpage} % total number of pages
\RequirePackage{scrlayer-scrpage}
% \RequirePackage[
%   a4paper,
%   11pt,
%   DIV=15
% ]{geometry}

% \pagestyle{scrheadings} % use KOMA-Script scrlayer-scrpage page style
\clearpairofpagestyles % clear placeholders for header and footer

\setkomafont{pageheadfoot}{\sffamily\small}
\setkomafont{pagenumber}{\sffamily\small}

% \ihead{
%   \begin{tabular}{l l}
%     \@student
%   \end{tabular}
% }
% \chead{\@assignment}
% \ohead{\@university\\\unidate\@deadline}
% \ifoot{\@coursetitle}
\ofoot{\thepage~von~\pageref{LastPage}}
