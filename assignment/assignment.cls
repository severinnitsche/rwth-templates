\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{assignment}

\LoadClass{scrartcl} % KOMA-Script article class

%% Student information
\edef\@student{}
\newcommand{\student}[2]{\ifx\@student\empty\edef\@student{#1 & #2}\else\edef\@student{#1 & #2 \\\@student{}}\fi}

%% Course information
\newcommand{\@university}{RWTH Aachen University}
\newcommand{\@coursetitle}{{\em No course title specified}}
\newcommand{\@assignment}{{\em No assignment title specified}}

\newcommand{\university}[1]{\renewcommand{\@university}{#1}}
\newcommand{\coursetitle}[1]{\renewcommand{\@coursetitle}{#1}}
\newcommand{\assignment}[1]{\renewcommand{\@assignment}{#1}}

%% Date format
\RequirePackage[ngerman]{babel}
\RequirePackage{datetime}
\RequirePackage{expl3}
\RequirePackage{xparse}
\ExplSyntaxOn
\NewExpandableDocumentCommand{\lasttwoofyear}{m}
 {% #1 is the offset
  \egreg_lasttwo:f { \int_to_arabic:n { #1 } }
 }
\cs_new:Nn \egreg_lasttwo:n
 {
  \tl_range:nnn { #1 } { -2 } { -1 }
 }
\cs_generate_variant:Nn \egreg_lasttwo:n { f }
\ExplSyntaxOff
\newcommand{\@semester}{%
  \ifnum\THEMONTH<4%
    WS~\number\numexpr\THEYEAR-1\relax/\lasttwoofyear{\THEYEAR}
  \else\ifnum\THEMONTH<10%
    SS~\THEYEAR
  \else
    WS~\THEYEAR/\lasttwoofyear{\THEYEAR+1}
  \fi\fi%
}
\newcommand{\@deadline}{\today}
\newcommand{\deadline}[3]{\renewcommand{\@deadline}{\formatdate{#1}{#2}{#3}}}
\newdateformat{unidate}{\THEDAY.~\monthname[\THEMONTH],~\@semester}

%% Typesetting
\renewcommand{\familydefault}{\sfdefault}
\renewcommand{\theequation}{\arabic{enumi}-\arabic{equation}}

%% Task environment
\newcommand{\@task}{Aufgabe}
\newcommand{\taskDescription}[1]{\renewcommand{\@task}{#1}}

\newcommand{\task}[2][]{%
  \ifthenelse{\equal{\the\@enumdepth}{1}}{\item[\@task\ \ifstrempty{#1}{\addtocounter{enumi}{1}}{\setcounter{enumi}{#1}}\labelenumi]}{}%
  \ifthenelse{\equal{\the\@enumdepth}{2}}{\item[\bfseries\ \ifstrempty{#1}{\addtocounter{enumii}{1}}{\setcounter{enumii}{#1}}\labelenumii]}{}%
  \ifthenelse{\equal{\the\@enumdepth}{3}}{\item[\bfseries\ \ifstrempty{#1}{\addtocounter{enumiii}{1}}{\setcounter{enumiii}{#1}}\labelenumiii]}{}%
  \ifthenelse{\equal{\the\@enumdepth}{4}}{\item[\bfseries\ \ifstrempty{#1}{\addtocounter{enumiv}{1}}{\setcounter{enumiv}{#1}}\labelenumiv]}{}%
  {#2}\par%
}

\newenvironment{tasks}{%
  \advance\@enumdepth by 1%
  \ifthenelse{\equal{\the\@enumdepth}{1}}{
    \setcounter{enumi}{0}%
    \setcounter{equation}{0}%
  }{}%
  \ifthenelse{\equal{\the\@enumdepth}{2}}{\setcounter{enumii}{0}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{3}}{\setcounter{enumiii}{0}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{4}}{\setcounter{enumiv}{0}}{}%
  \begin{list}{%
    \ifthenelse{\equal{\the\@enumdepth}{1}}{\addtocounter{enumi}{1}\labelenumi}{}%
    \ifthenelse{\equal{\the\@enumdepth}{2}}{\addtocounter{enumii}{1}\labelenumii}{}%
    \ifthenelse{\equal{\the\@enumdepth}{3}}{\addtocounter{enumiii}{1}\labelenumiii}{}%
    \ifthenelse{\equal{\the\@enumdepth}{4}}{\addtocounter{enumiv}{1}\labelenumiv}{}%
  }{}
}{%
  \end{list}%
  \advance\@enumdepth by -1%
}

%% simplification
\renewcommand{\section}[2][]{
  \ifthenelse{\equal{\the\@enumdepth}{4}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{3}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{2}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{0}}{\begin{tasks}}{}%
  \task[#1]{#2}%
}

\renewcommand{\subsection}[2][]{
  \ifthenelse{\equal{\the\@enumdepth}{4}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{3}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{1}}{\begin{tasks}}{}%
  \task[#1]{#2}
}

\renewcommand{\subsubsection}[2][]{
  \ifthenelse{\equal{\the\@enumdepth}{4}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{2}}{\begin{tasks}}{}%
  \task[#1]{#2}
}

\AtEndDocument{
  \ifthenelse{\equal{\the\@enumdepth}{4}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{3}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{2}}{\end{tasks}}{}%
  \ifthenelse{\equal{\the\@enumdepth}{1}}{\end{tasks}}{}%
}

%% Page style
\RequirePackage{totpages} % total number of pages
\RequirePackage[headsepline,footsepline]{scrlayer-scrpage}
\RequirePackage[
  a4paper,
  inner=2cm,
  outer=2cm,
  top=3.5cm,
  bottom=2.5cm,
  head=2cm,
]{geometry}

\pagestyle{scrheadings} % use KOMA-Script scrlayer-scrpage page style
\clearpairofpagestyles % clear placeholders for header and footer

\setkomafont{pageheadfoot}{\sffamily\small}
\setkomafont{pagenumber}{\sffamily\small}

\ihead{
  \begin{tabular}{l l}
    \@student
  \end{tabular}
}
\chead{\@assignment}
\ohead{\@university\\\unidate\@deadline}
\ifoot{\@coursetitle}
\ofoot{\thepage~von~\ref{TotPages}}
