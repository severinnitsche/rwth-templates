# Assignment
Diese LaTeX-Vorlage ist zur Abgabe von Hausarbeiten an deutschen Hochschulen o.ä. gedacht. Sie versieht Kopf- und Fußzeile mit entsprechender Numerierung, Namensfeldern, Datum und Kursname, sowie Abgabetitel.

## Allgemeine Informationen
Aufgrund mangelnder Übersetzungsfähigkeiten ist die Sprache der Vorlage ausschließlich deutsch. Eine Übersetzung ist nicht vorgesehen.

Aggregierte Befehle, etwa aus `amsmath`, etc., sind nicht mehr in der Klasse enthalten. Hierfür wird das Paket `assignment.sty` bereitgestellt, dass diese aggregiert und ggf. angemessene Kürzel oder neudefinitionen enthält.

## Verwendung
### Befehle
- `\coursetitle{<Kurs>}` legt den Kursnamen fest.
- `\assignment{<Titel>}` legt den Titel der Hausarbeit fest.
- `\[[sub]sub]section[<Nummer>]{<Aufgabenstellung>}` fügt eine neue Aufgabe hinzu. Kann ohne `tasks` verwendet werden.
- `\student{<Name>}{<Matr.-Nr.>}` erlaubt es mehrere beteiligte Studenten anzugeben
- `\university{<Uni>}` legt die Hochschule fest. Standardwert `RWTH Aachen University`.
- `\deadline` legt die Abgabefrist fest. Standardwert `\today`.
- `\taskDescription{<Aufgabe>}` legt die anzeige von Aufgaben als `Aufgabe` fest. Standardwert `Aufgabe`.
- `\unidate` ist eine Datumsformat der Form `d. mmmm, [SS YYYY | WS YY/YY]`
- `\task[<Nummer>]{<Aufgabenstellung>}` fügt eine neue Aufgabe hinzu. Muss mit `tasks` verwendet werden.

### Umgebungen
- `tasks` für die Verwendung mit `\task[<Nummer>]{<Aufgabenstellung>}`. 