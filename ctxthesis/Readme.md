# Thesis

This thesis template is made for my Bachelors thesis and may work for master thesises and other work as well.

## Usage
The main tex file is `skeleton/thesis` and not to be touched.
Compile with `context sekeleton/thesis`.

The main user file is `thesis` and contains as intended content calls
```
\component sections/what
\component sections/ever
\component sections/you
\component sections/fancy
```
I leave it up to you to find out where the sections go.
Look at the examples for a good setup.

There is also a user environment file `environments/macros` intended to define macros you frequently use.

In the body environment I specified some handy environments:
```
\theorem Lorem Ipsum
\proposition Dolor sit amet
\lemma consectur de multo
\corollary ancio de casa
\conjecture y egerect
\claim vamos a la playa
\definition dolce vita
\remark Vincenco almoso
\proof q.e.d.
```

It is also handy to compile only a single component.

The header info goes into `meta`.
```
\meta[
  title={Prädikatenlogik mit Mehrheits- und\\ Zählquantoren},
  type=BA,
  author={Severin Leonard Christian Nitsche, 434 356},
  deadline={d=2,m=1,y=1970}]
```
Your abstract (if needed) into `abstract`.

References go to `references.bib`.
